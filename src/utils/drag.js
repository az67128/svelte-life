export default function drag(node) {
  let y;

  function handleMousedown(e) {
    const event = e.touches ? e.touches[0] : e;
    y = event.clientY;
    node.dispatchEvent(
      new CustomEvent("dragstart", {
        detail: { y }
      })
    );

    window.addEventListener("mousemove", handleMousemove);
    window.addEventListener("touchmove", handleMousemove);
    window.addEventListener("mouseup", handleMouseup);
    window.addEventListener("touchend", handleMouseup);
  }

  function handleMousemove(e) {
    const event = e.touches ? e.touches[0] : e;
    const dy = event.clientY - y;
    node.dispatchEvent(
      new CustomEvent("dragmove", {
        detail: { y, dy }
      })
    );
  }

  function handleMouseup(e) {
    const event = e.changedTouches ? e.changedTouches[0] : e;
    const dy = event.clientY - y;
    node.dispatchEvent(
      new CustomEvent("dragend", {
        detail: { y, dy }
      })
    );
    window.removeEventListener("mousemove", handleMousemove);
    window.removeEventListener("touchmove", handleMousemove);
    window.removeEventListener("mouseup", handleMouseup);
    window.removeEventListener("touchend", handleMouseup);
  }

  node.addEventListener("mousedown", handleMousedown);
  node.addEventListener("touchstart", handleMousedown);

  return {
    destroy() {
      node.removeEventListener("mousedown", handleMousedown);
      node.removeEventListener("touchstart", handleMousedown);
    }
  };
}
