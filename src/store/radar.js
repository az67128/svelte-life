import { writable } from "svelte/store";
import { derived } from "svelte/store";
import { sectors } from "../constants";

const localStorage = process.browser ? window.localStorage : {getItem:()=>{}};
const defaultRadar = sectors.map(group => ({
  ...group,
  value: 0
}));

export const radar = (() => {
  const localStorageData = localStorage.getItem("radar");
  const { subscribe, update } = writable(
    localStorageData ? JSON.parse(localStorageData) : defaultRadar
  );

  return {
    subscribe,
    set: (id, value) =>
      update(store => {
        const radarData = store.map(item =>
          item.id === id ? { ...item, value: parseFloat(value) } : item
        );
        localStorage.setItem("radar", JSON.stringify(radarData));
        return radarData;
      })
  };
})();

export const activeSector = writable(null);

export const balance = derived(radar, $radar => {
  const radarBalance = $radar.reduce(
    (res, item) => {
      return {
        min: res.min > item.value ? item.value : res.min,
        max: res.max < item.value ? item.value : res.max
      };
    },
    { min: 10, max: 0 }
  );

  return radarBalance.max - radarBalance.min;
});
