import { writable, derived, get } from "svelte/store";
import { sectors } from "../constants";
import { LATEST_VISIBLE_DATE } from "../constants";
import nanoid from "nanoid";

const localStorage = process.browser ? window.localStorage : {getItem:()=>{}};
export const todo = (() => {
  const lsTodo = localStorage.getItem("todo");
  const store = writable(
    lsTodo
      ? JSON.parse(lsTodo).map(item => ({
          ...item,
          date: new Date(item.date),
          lastChange: new Date(item.lastChange)
        }))
      : []
  );
  const update = fn => {
    store.update(state => {
      const newState = fn(state);
      localStorage.setItem("todo", JSON.stringify(newState));
      return newState;
    });
  };
  return {
    subscribe: store.subscribe,
    add: (group, text) =>
      update(state => {
        state.push({
          id: nanoid(),
          group,
          text,
          date: new Date(),
          lastChange: new Date(),
          complete: false
        });
        return state;
      }),
    toggle: id =>
      update(state => {
        const todo = state.find(item => item.id === id);
        todo.complete = !todo.complete;
        todo.lastChange = new Date();
        return state;
      }),
    update: (id, text) =>
      update(state => {
        const todo = state.find(item => item.id === id);
        todo.text = text;
        todo.lastChange = new Date();
        return state;
      }),
    delete: id => update(state => state.filter(item => item.id !== id))
  };
})();

export const showAll = (() => {
  const { subscribe, update } = writable(false);
  return {
    subscribe,
    toggle: () => update(state => !state)
  };
})();

export const todoGroups = derived([todo, showAll], ([$todo, $showAll]) => {
  const latestVisible = new Date();
  latestVisible.setDate(latestVisible.getDate() - LATEST_VISIBLE_DATE);

  return sectors.map(group => ({
    ...group,
    todo: $todo
      .map(item => ({
        ...item,
        opacity: item.complete
          ? 1 - (new Date() - item.lastChange) / (7 * 1000 * 60 * 60 * 24)
          : 1
      }))
      .filter(
        item =>
          item.group === group.id &&
          ($showAll || item.lastChange > latestVisible)
      )
      .sort((a, b) => {
        if (a.complete && !b.complete) return 1;
        if (!a.complete && b.complete) return -1;
        return a.date > b.date ? -1 : 1;
      })
  }));
});

export const editTodo = (() => {
  const initialState = {
    id: null,
    groupId: null,
    text: null
  };
  const { subscribe, update, set } = writable(initialState);
  return {
    subscribe,
    start: targetId => {
      const { id, text } = get(todo).find(item => item.id === targetId);
      set({ id, text });
    },
    new: groupId => set({ ...initialState, groupId }),
    change: e =>
      update(state => {
        state.text = e.target.value;
        return state;
      }),
    save: () =>
      update(state => {
        if (state.id && state.text) todo.update(state.id, state.text);
        if (state.groupId && state.text) todo.add(state.groupId, state.text);
        return initialState;
      }),
    delete: () =>
      update(state => {
        if (state.id) todo.delete(state.id);
        return initialState;
      })
  };
})();
