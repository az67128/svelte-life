export const sectors = [
  {
    id: "hobby",
    name: "ХОББИ"
  },
  {
    id: "friendship",
    name: "ДРУЖБА"
  },
  {
    id: "health",
    name: "ЗДОРОВЬЕ"
  },
  {
    id: "job",
    name: "РАБОТА"
  },
  {
    id: "love",
    name: "ЛЮБОВЬ"
  },
  {
    id: "money",
    name: "БЛАГОСОСТОЯНИЕ"
  }
];

export const LATEST_VISIBLE_DATE = 1;
